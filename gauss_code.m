% code credit: Julia Pluta
clear;

file_path = 'C:\Users\asiar\Downloads\Pomiary_Gauss.csv';
fid = fopen(file_path);
C = textscan(fid, '%q %q','Delimiter', ';');
measurements = 750;

for j=1:measurements
    A(j,1) = str2num(cell2mat(C{1}(j)));
end

% Sharpino-Wilk test:
S = swtest(A)

%eGauss:
h = histfit(A);
xlabel('Liczba zliczen zarejestrowanych w ciagu t = 10 s');
ylabel('Liczba powtorzen');
title('Histogram');